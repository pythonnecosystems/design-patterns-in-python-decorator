# Python 디자인 패턴: 데코레이터

## 데토레이터 패턴의 이해
데코레이터 디자인 패턴은 같은 클래스의 다른 객체의 동작에 영향을 주지 않고 개별 객체에 정적 또는 동적으로 동작을 추가하는 구조적 패턴이다.

즉, 객체의 구조를 수정하지 않고 새로운 기능을 추가하여 객체를 "꾸미거나" 기능을 향상시킬 수 있다.

이 패턴은 유연하고 재사용 가능한 방식으로 객체의 기능을 확장하고자 할 때 유용하다.

### 데코레이터 패턴 사용 시기

1. **동적으로 책임 추가하기**: *코드를 수정하지 않고 런타임에 객체의 동작이나 기능을 동적으로 추가하고자 하는 경우*.
1. **복잡한 상속 계층 구조 피하기**: *유지 관리가 어려울 수 있는 서브클래싱을 통해 깊고 복잡한 클래스 계층 구조를 파하고자 하는 경우*.

### 실제 예: 게임 캐릭터 능력
게임 디자인에서 다양한 캐릭터가 고유한 능력을 가지고 있는 시나리오를 생각해 보겠다. 게임이 발전함에 따라 새로운 능력을 추가해야 한다.

**더블 데미지 데코레이터**, **파이어볼 데코레이터**, **인비저빌리티 데코레이터** 등 같은 게임 캐릭터 능력을 예로 들어 보자. 캐릭터는 언제든지 이러한 어빌리티의 다양한 조합을 가질 수 있다.

모든 서브클래스 조합을 상속으로 처리하면 코드가 복잡해집니다. 데코레이터 패턴은 이를 단순화하여 캐릭터에 능력을 추가하는 동시에 코드를 깔끔하고 유연하게 유지할 수 있다.

## 데코레이터 패턴의 주요 구성 요소
데코레이터 패턴을 사용하면 다른 객체를 수정하지 않고 객체에 동작을 추가할 수 있다. 정확히 사용하려면 아래 나열하는 필수적인 부분을 알아야 한다.

1. **컴포넌트**: 구체적인 컴포넌트와 데코레이터가 공유하는 기반이다. 확장할 수 있는 핵심 기능을 정의한다.
1. **구체적인 컴포넌트**: 추가 동작을 더하는 기본 객체이다. 데코레이터가 이를 감싸고 있다.
1. **데코레이터**: Component 인터페이스를 따르는 추상 클래스. 컴포넌트에 연결하고 코어에 특정 기능을 추가한다.
1. **구체적인 데코레이터**: Decorator 추상 클래스를 확장하여 특정 동작을 구현하는 클래스이다. 이를 스택하여 데코레이터 체인을 만들 수 있다.

이 부분은 데코레이터 패턴을 효과적으로 사용하여 코드를 깔끔하고 관리하기 쉽게 유지하면서 객체에 기능을 동적으로 추가하거나 제거할 수 있도록 하는 데 매우 중요하다.

![](./images/1_DRHITbGhjSL50mIwCRMBDw.webp)

## Python에서 데코레이터 패턴 구현하기
이 섹션에서는 캐릭터의 능력을 강화하는 데 데코레이터 디자인 패턴의 역할을 보이도록 하겠다. 기본 캐릭터에 Double Damaged와 Invisibility와 같은 데코레이터를 동적으로 적용하여 복합적인 힘을 가진 특별한 캐릭터를 만들려고 한다.

### Step 1: 컴포넌트
컴포넌트는 강화할 수 있는 핵심 동작을 정의하는 기본 인터페이스이다. 이 예에서는 기본 공격 방법을 가진 게임 캐릭터를 나타낸다.

```python
from abc import ABC, abstractmethod

# Step 1: Component - The base game character
class Character(ABC):
    @abstractmethod
    def get_description(self):
        pass

    @abstractmethod
    def get_damage(self):
        pass
```

### Step 2: 구체적인 컴포넌트
구체적인 컴포넌트는 특수 능력을 추가할 수 있는 기본 게임 캐릭터이다. 이 단계에서는 기본 캐릭터 클래스를 생성한다.

```python
# Step 2: Concrete Component - Basic game character
class BasicCharacter(Character):
    def get_description(self):
        return "Basic Character"

    def get_damage(self):
        return 10
```

### Step 3: 데코레이터
데코레이터는 컴포넌트 인터페이스도 구현하는 추상 클래스이다. 이 클래스는 컴포넌트 객체에 대한 참조를 가지며 컴포넌트의 코어에 특정 동작을 추가한다. 이 단계에서는 추상 데코레이터 클래스를 생성한다.

```python
# Step 3: Decorator - Abstract decorator class
class CharacterDecorator(Character, ABC):
    def __init__(self, character):
        self._character = character

    @abstractmethod
    def get_description(self):
        pass

    @abstractmethod
    def get_damage(self):
        pass
```

### Step 4: 구체적인 데코레이터
구체적인 데코레이터는 데코레이터 추상 클래스를 확장하여 특정 동작을 구현하는 클래스이다. 이 단계에서는 Double Damaged와 같은 특수 캐릭터 능력에 대한 구체적인 데코레이터 클래스를 생성한다.

```python
# Step 4: Concrete Decorator
class DoubleDamageDecorator(CharacterDecorator):
    def get_description(self):
        return self._character.get_description() + " with Double Damage"

    def get_damage(self):
        return self._character.get_damage() * 2
    
class FireballDecorator(CharacterDecorator):
    def get_description(self):
        return self._character.get_description() + " with Fireball"

    def get_damage(self):
        return self._character.get_damage() + 20
    
class InvisibilityDecorator(CharacterDecorator):
    def get_description(self):
        return self._character.get_description() + " with Invisibility"

    def get_damage(self):
        return self._character.get_damage()
```

### Step 5: 클라이언트 코드
클라이언트는 데코레이터를 생성하고 구성하여 핵심 캐릭터와 함께 사용할 책임이 있다. 이 단계에서는 다양한 능력을 가진 캐릭터를 만드는 방법을 설명한다.

```python
# Step 5: Client Code - Creating a character with abilities

if __name__ == "__main__":
    character = BasicCharacter()
    print(character.get_description())  # Output: "Basic Character"
    print(character.get_damage())       # Output: 10

    # Create different decorators
    double_damage_decorator = DoubleDamageDecorator(character)
    fireball_decorator = FireballDecorator(character)
    invisibility_decorator = InvisibilityDecorator(character)

    # Apply decorators individually
    print(double_damage_decorator.get_description())  # Output: "Basic Character with Double Damage"
    print(double_damage_decorator.get_damage())       # Output: 20

    print(fireball_decorator.get_description())  # Output: "Basic Character with Fireball"
    print(fireball_decorator.get_damage())       # Output: 30

    print(invisibility_decorator.get_description())  # Output: "Basic Character with Invisibility"
    print(invisibility_decorator.get_damage())       # Output: 10

    # Combine decorators
    double_fireball_character = DoubleDamageDecorator(FireballDecorator(character))
    print(double_fireball_character.get_description())  # Output: "Basic Character with Double Damage with Fireball"
    print(double_fireball_character.get_damage())       # Output: 60

    invisibility_double_fireball_character = InvisibilityDecorator(double_fireball_character)
    print(invisibility_double_fireball_character.get_description())  # Output: "Basic Character with Invisibility with Double Damage with Fireball"
    print(invisibility_double_fireball_character.get_damage())       # Output: 60
```

이 코드 구조를 사용하면 캐릭터에 여러 데코레이터를 중첩하여 능력을 강화할 수 있다. 데코레이터는 적용될 때마다 캐릭터의 설명과 대미지를 수정한다.

## 데코레이터 패턴의 장점
데코레이터 디자인 패턴은 몇 가지 주목할 만한 장점을 제공한다.

1. **서브클래스가 필요 없는 확장성**: 새로운 서브클래스를 만들지 않고도 객체에 더 많은 기능을 추가할 수 있다. 따라서 코드가 더 유연해지고 기존 코드를 변경하지 않아도 된다.
1. **런타임 동작 변경**: 데코레이터를 사용하면 프로그램이 실행되는 동안 객체의 동작을 변경할 수 있다. 필요에 따라 객체가 수행할 수 있는 작업을 추가하거나 제거할 수 있다.
1. **능력 결합**: 객체 주위에 여러 개의 데코레이터를 배치하여 서로 다른 능력을 결합할 수 있다. 이렇게 하면 복잡한 커스텀 객체를 간단하게 만들 수 있다.
1. **단일 책임 원칙**: 데코레이터 패턴은 각 클래스가 한 가지 일만 잘해야 한다는 규칙을 따른다. 따라서 코드가 더 깔끔하고 관리하기 쉬워진다.

## 고려 사항과 잠재적 단점
데코레이터 디자인 패턴은 많은 장점을 제공하지만 몇 가지 단점도 있다.

1. **작은 요소의 과다**: 데코레이터를 구현하면 디자인에 작은 요소가 많아질 수 있다. 이로 인해 수많은 데코레이터 클래스가 생겨 전체 디자인을 관리하고 이해하기 어려워질 수 있다.
1. **복잡성과 학습 곡선**: 데코레이터 패턴은 초보자에게 적합하지 않다. 객체 지향 프로그래밍 원리에 대한 탄탄한 이해가 필요하며, 초보자는 처음에 이해하기 어려울 수 있다.
1. **디버깅의 어려움**: 확장된 데코레이터 구성 요소로 인해 디버깅이 어려울 수 있다. 복잡한 데코레이터 스택에서 문제의 원인을 파악하려면 추가적인 노력과 세심한 검사가 필요할 수 있다.
1. **잠재적인 높은 복잡성**: 데코레이터 패턴을 사용하는 시스템의 아키텍처는 특히 많은 데코레이터와 이들 간의 복잡한 상호 작용이 있는 경우 매우 복잡해질 수 있다. 이러한 복잡성 증가는 시스템을 유지 관리하고 이해하기 어렵게 만들 수 있다.

## 피해야 할 일반적인 실수
데코레이터 패턴으로 작업할 때는 견고한 구현을 위해 흔히 저지르는 실수를 알아두는 것이 중요하다.

1. **복잡한 데코레이터**: 너무 많은 작업을 처리하는 지나치게 복잡한 데코레이터를 피하여야 한다. 각 데코레이터는 특정 목적을 위해 사용되어야 하며, 지나치게 복잡해지지 않도록 해야 한다.
1. **데코레이터 순서가 중요하다**: 데코레이터를 적용하는 순서는 매우 중요하다. 데코레이터를 논리적 순서대로 적용하면 원하는 동작을 얻을 수 있다.
1. **베이스 클래스 무시**: 게임 캐릭터와 같은 베이스 클래스는 가능한 한 단순하게 유지하여야 한다. 이를 무시하면 불필요한 코드로 복잡해질 수 있다.

## Python 데코레이터와 데코레이터 디자인 패턴

### Python 함수 데코레이터
Python 함수 데코레이터는 함수나 메서드의 동작을 수정할 수 있는 Python 프로그래밍 언어의 기능이다. 소스 코드를 수정하지 않고 기존 함수에 기능을 추가하는 데 사용된다.

Python 함수 데코레이터는 기본적으로 다른 함수를 인수로 받아 그 동작을 확장하고 새로운 함수를 반환하는 함수이다. 로깅, 캐싱 또는 인증과 같은 기능을 구현할 수 있는 Python 방식이다

### 코드 예: Python 함수 데코레이터

```python
def my_decorator(func):
    def wrapper():
        print("Something is happening before the function is called.")
        func()
        print("Something is happening after the function is called.")
    return wrapper

@my_decorator
def say_hello():
    print("Hello!")

say_hello()
```

이 예에서 `my_decorator` 함수는 `say_hello` 함수를 래핑하여 호출 전후에 동작을 추가하는 Python 함수 데코레이터이다.

### 주요 차이점
요약하자면, Python 함수 데코레이터는 개별 함수를 수정하는 데 중점을 두는 반면, 데코레이터 디자인 패턴은 데코레이터 구성을 통해 객체의 동작을 향상하고 확장하는 데 사용된다.

### 목적
- **Python 함수 데코레이터**: 주로 함수나 메서드의 동작을 수정하는 데 사용된다.
- **데코레이터 디자인 패턴**: 객체나 클래스의 기능을 동적으로 향상시키는 데 사용된다.

### 적용
- **Python 함수 데코레이터**: 일반적으로 단일 함수 또는 메서드 내에서 로깅, 캐싱 또는 인증과 같은 문제 해결에 사용된다.
- **데코레이터 디자인 패턴**: 여러 데코레이터를 쌓아 객체의 동작을 확장하는 데 사용된다.

### 상속
- **Python 함수 데코레이터**: 단일 함수의 동작을 직접 확장하므로 상속을 사용하지 않는다.
- **데코레이터 디자인 패턴**: 여러 데코레이터를 특정 순서대로 객체나 클래스에 적용할 수 있는 유연성을 제공한다.

## 다른 패턴과의 관계 - 요약
데코레이터 패턴은 각각 고유한 목적을 가진 다른 디자인 패턴과 연결되어 있다.

### 데코레이터 대 어댑터
- **인터페이스 변경**: 어댑터는 객체의 인터페이스를 완전히 변환하는 반면, 데코레이터는 일반적으로 인터페이스를 확장하거나 유지한다. 또한 데코레이터는 재귀적 구성을 지원하지만 어댑터는 지원하지 않는다.

### 데코레이터 대 프록시
- **인터페이스 처리**: 프록시와 데코레이터는 모두 객체를 향상시키지만 인터페이스 처리 방식이 다르다. 프록시는 인터페이스를 변경하지 않고 스탠드인 역할을 하는 반면, 데코레이터는 인터페이스를 확장하여 기능을 추가한다.
- **컴포지션 제어**: 구조적 유사성에도 불구하고 프록시와 데코레이터는 의도가 다르다. 프록시는 객체 수명 주기를 처리하는 반면, 데코레이터는 클라이언트 제어를 통해 기능을 추가하는 데 중점을 둔다.

### 데코레이터 대 책임 사슬
- **구성과 실행**: 책임 사슬과 데코레이터는 비슷한 구조를 공유하며, 실행을 위해 구성에 의존한다. 책임 사슬에서 핸들러는 독립적으로 작업하고 요청을 중지할 수 있다. 데코레이터는 흐름을 유지하면서 동작을 확장한다.-

### 데코레이터와 컴포지트
- **구조체의 컴포지션**: 컴포지트와 데코레이터는 컴포지션과 유사한 구조를 사용한다. 하지만 데코레이터는 객체에 책임을 추가하는 반면, 컴포지트은 결과를 집계한다.
- **협력**: 데코레이터와 컴포지트는 협력할 수 있으며, 데코레이터는 컴포지트 구조 내의 특정 오브젝트를 향상시킬 수 있다.

### 전략 패턴
- **스킨과 배짱 변경**: 데코레이터는 '스킨' 또는 추가 책임을 변경하는 반면, 전략은 '배짱' 또는 핵심 행동을 변경한다.

## 마치며
데코레이터 패턴은 객체 기능을 동적으로 향상시켜 Python에서 유연성과 코드 재사용성을 제공한다.

이 포스팅에서는 더욱 우아하고 확장 가능한 소프트웨어 설계를 위한 데코레이터 패턴의 원리, 적용 사례 및 Python에서 실용적인 구현 방법을 소개했다.
