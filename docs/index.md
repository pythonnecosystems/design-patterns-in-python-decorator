# Python 디자인 패턴: 데코레이터  <sup>[1](#footnote_1)</sup>

> *반복되는 코딩 문제에 직면한 적이 있지요? 이미 검증된 솔루션으로 구성된 도구 상자가 있다고 상상해 보자. 바로 디자인 패턴이 제공하는 솔루션이다. 이 시리즈에서는 이러한 패턴이 무엇이며 어떻게 코딩 기술을 향상시킬 수 있는지 살펴본다.*

<a name="footnote_1">1</a>: [Design Patterns in Python: A Series](https://medium.com/@amirm.lavasani/design-patterns-in-python-a-series-f502b7804ae5)를 편역한 것이다.
